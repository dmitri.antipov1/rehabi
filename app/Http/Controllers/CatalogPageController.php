<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Catalog;

class CatalogPageController extends Controller

{

    public function show(){

        $catalogs = Catalog::all();


        return view('site.catalog', compact('catalogs'));
    }

    public function showCatalog(Catalog $catalog) {



      return view('site.showcatalog', [
          'catalog' => $catalog
      ]);

    }


}
