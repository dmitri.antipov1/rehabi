<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Juhendid;
use App\Video;

class InfoPageController extends Controller
{
    public function show(){

        return view('site.info');
    }

    public function showRules(){


        return view('info.rules');
    }

    public function showProtsess(){

        return view('info.process');
    }

     public function showJuhendid(){

        $juhendid = Juhendid::all();

         return view('info.juhendid',[
             'juhendid' => $juhendid
         ]);
    }

    public function showBlog(){

        $videos = Video::all();

        return view('info.vblog',[
            'videos' => $videos
        ]);
    }

}
