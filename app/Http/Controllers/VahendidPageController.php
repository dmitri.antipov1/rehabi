<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vahendid;

class VahendidPageController extends Controller
{
    public function show(){

        $vahendid = Vahendid::all();

        return view('site.vahendid', [
            'vahendid' => $vahendid
        ]);
    }
}
