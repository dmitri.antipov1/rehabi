<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Catalog;

class AdminPageController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(){
        if (view()->exists('admin.index')) {


            return view('admin.index');
        }
    }

    public function addCataloog(){

        if (view()->exists('admin.addcataloog')) {

            $catalogData = Catalog::all();

            return view('admin.addcataloog', [
                'catalogData' => $catalogData
            ]);

        }



    }
}
