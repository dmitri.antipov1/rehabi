<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('web')->group(function (){

    Route::get('/', 'IndexController@show');

    Route::prefix('catalog')->group(function(){
        Route::get('/', 'CatalogPageController@show')->name('catalog');
        Route::get('/{catalog}', 'CatalogPageController@showCatalog')->name('showcatalog');
    });

    Route::prefix('info')->group(function(){
    Route::get('/', 'InfoPageController@show');
    Route::get('/rules', 'InfoPageController@showRules')->name('rules');
    Route::get('/valmistamisprotsess', 'InfoPageController@showProtsess')->name('valmistamisprotsess');
    Route::get('/juhendid', 'InfoPageController@showJuhendid')->name('juhendid');
    Route::get('/videoblog', 'InfoPageController@showBlog')->name('videoblog');
    });

    Route::get('contacts', 'ContactsPageController@show');

    Route::get('hooldusvahendid', 'VahendidPageController@show')->name('vahendid');


});


//admin

<<<<<<< HEAD
Route::middleware('auth')->group(function(){
        Route::prefix('admin')->group(function(){
            Route::get('/', 'AdminPageController@show');
        });
});
=======
>>>>>>> a01b388088e495282c3e87e50f697075ed83c32e

Auth::routes();

Route::prefix('admin')->group(function(){
    Route::get('/', 'AdminPageController@index')->name('admin');
    Route::get('/addcataloog', 'AdminPageController@addCataloog')->name('addCataloog');

});

