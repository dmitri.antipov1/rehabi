@extends('layout.site')

@section('contant')

    <section class="lable">
        <img class="lable_img" src="{{asset('/assets/img/fon1.jpg')}}" />
        <img class="lable_img-mobile" src="{{asset('/assets/img/fon2.jpg')}}" />
        <div class="lable_inner">
            <div class="lable_inner_text">
               <i>Eritellimusel valmistatud</i>
            </div>
            <div class="lable_inner_title">
                Esteetilised Proteesid
            </div>
        </div>
    </section>

    <!------- KIRJELDUS ---------->

    <section class="items">
        <div class="container">
            <div class="items_inner">
                <a href="{{ url('http://rehabi.ee/catalog/1')}}">
                <div class="item">
                    <div class="item_text">
                        Sõrmeproteesid
                    </div>
                    <img src="{{asset('assets/img/K11.jpg')}}" alt="" class="item_img">
                </div>
                </a>
                <a href="{{ url('http://rehabi.ee/catalog/2')}}">
                <div class="item">
                    <div class="item_text">
                        Käeosade proteesid
                    </div>
                    <img src="{{asset('assets/img/K22.jpg')}}" alt="" class="item_img">
                </div>
                </a>
                <a href="{{ url('http://rehabi.ee/catalog/3')}}">
                <div class="item">
                    <div class="item_text">
                        Jalaosade proteesid
                    </div>
                    <img src="{{asset('assets/img/K33.jpg')}}" alt="" class="item_img">
                </div>
                </a>
                <a href="{{ url('http://rehabi.ee/catalog/4')}}">
                <div class="item">
                    <div class="item_text">
                        3D lahendused
                    </div>
                    <img src="{{asset('assets/img/K44.jpg')}}" alt="" class="item_img">
                </div>
                </a>

            </div>
        </div>
    </section>
    @endsection
