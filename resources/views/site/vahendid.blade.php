@extends('layout.site')

@section('contant')

    <section class="vahendid">
        <div class="container">
            <div class="line"></div>
            @foreach($vahendid as $vahend)
            <div class="vahendid_inner">
                <div class="vahendid_img">
                    <img src="assets/img/{{$vahend->img}}" alt="">
                </div>
                <div class="vahendid_kirjeldus">
                    <div class="vahendid_name">
                        {{$vahend->name}}
                    </div>
                    <div class="vahendid_descr">
                        {{$vahend->descr}}
                    </div>
                    <div class="vahendid_price">
                        {{$vahend->hind}}
                    </div>
                </div>
            </div>
                <div class="line"></div>
                @endforeach
        </div>
    </section>

    @endsection
