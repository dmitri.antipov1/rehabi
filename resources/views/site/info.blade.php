@extends('layout.site')

@section('contant')

    <section class="info">
        <div class="container">
            <div class="line"></div>
            <div class="info_list">
                <ul class="info_btn">

                    <a href="{{route('rules')}}"> <li>Esteetilisele proteesile hüvitise saamine Sotsiaalkindlustuseametist</li></a>

                    <a href="{{route('juhendid')}}"><li>Juhendid</li></a>
                    <a href="{{route('videoblog')}}"><li>Videoblogi</li></a>

                </ul>
            </div>
        </div>
    </section>

    @endsection
