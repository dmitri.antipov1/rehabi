@extends('layout.site')

@section('contant')
    <section class="contacts">
        <div class="container">
            <div class="line">

            </div>
            <div class="contacts_inner">
                <div class="contacts_data">
                    <ul>
                        <li>Rehabi Aesthetics OÜ</li>
                        <li class="line"></li>
                        <li id="contacts_phone-phone">
                            <ion-icon name="call-outline"></ion-icon>
                            <a href="tel:+37255641270">+372 55 64 12 70</a></li>
                        <li class="line"></li>
                        <li>
                            <ion-icon name="mail-outline"></ion-icon>
                            <a href="mailto:info@rehabi.ee">info@rehabi.ee</a></li>
                        <li class="line"></li>
                        <li>
                            <ion-icon name="location-outline"></ion-icon>
                            Tulika 19, Tallinn, 10613
                        </li>
                    </ul>
                </div>
                <div class="contacts_map">
                    <iframe
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2029.260014869681!2d24.71794071607632!3d59.428737881693664!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46929485838c9901%3A0x282b87813d12696a!2sTulika%2019%2C%2010613%20Tallinn!5e0!3m2!1sru!2see!4v1588835231575!5m2!1sru!2see"
                        width="542" height="320" frameborder="0" style="border:0;" allowfullscreen=""
                        aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>
            <div class="line"></div>
            <div class="contacts_vastuvott_inner">
                <div class="contacts_vastuvott_left">
                    <div class="contacts_vastuvott">
                        <div class="contacts_vastuvott_title">
                            Vastuvõtt
                        </div>
                        <div class="contacts_vastuvott_text">
                            <p>Vastuvõtule registeerumiseks helistage 55641270</p>
                            <p>Vastuvõtule palume alati ette registreerida. Kui te ei saa vastuvõtule tulla, siis palume sellest meile teada anda. Vastuvõtule on võimalik registreerida ka e-maili teel: Info@rehabi.ee </p>
                            <p>Proteesimeistri vastuvõtt toimub OÜ Rehabilitatsiooniabi töökojas Tallinnas, aadressil Tulika 19,esimesel korrusel (FLORA maja). Vajadusel teostame koduvisiite. Külastuse tingimustes ja asjaoludes on vaja proteesimeistriga täpsemalt kokku leppida. Koduvisiite teostame üle Eesti.</p>
                            <h3>Parkimine</h3>
                            <p>Maja ees või hoovis on võimalik tasuta parkida (sisenedes meieruumidesse on kindlasti vaja sekretärile edastada pargitud auto number või kasutada sissepääsu juures olevat parkimisautomaati).</p>
                        </div>
                    </div>
                </div>
                <div class="contacts_vastuvott_right">
                    <div class="contacts_vastuvott">
                        <div class="contacts_vastuvott_title">
                            Vastuvõtuajad
                        </div>
                        <div class="contacts_vastuvott_text">
                            <p>E-R 10.00- 17.00 L- P kokkuleppel</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
