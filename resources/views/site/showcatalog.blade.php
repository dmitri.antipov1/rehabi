@php
/**
 * @var \App\Catalog $catalog
*/
@endphp

@extends('layout.site')

@section('contant')

    <section class="toode">
        <div class="container">
            <div class="line"></div>
            <div class="toode_inner">
                <div class="toode_inner_title">
                    {!! $catalog->title !!}
                </div>
                <div class="toode_desc">
                    {!! $catalog->protsessid !!}
                </div>
                <div class="line"></div>
                <div class="toode_inner_etapid">
                    <div class="toode_inner_etapid_img">
                        <img src="{{asset('assets/img/'.$catalog->image_2)}}" alt="">
                    </div>
                    <div class="toode_inner_etapid_text">
                        {!! $catalog->val_etapid !!}
                    </div>
                </div>
                <div class="line"></div>
                <div class="toode_materjalid">
                    {!! $catalog->material !!}
                </div>
                <div class="toode_kinn">
                    {!! $catalog->kinnitan !!}
                </div>
            </div>
            <a href="{{route('catalog')}}" class="back_btn">Tagasi</a>
        </div>
    </section>


@endsection
