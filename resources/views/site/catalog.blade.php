@extends('layout.site')

@section('contant')

    <section class="catalog">
        <div class="container">
            <div class="line"></div>
            <div class="catalog_items">
                @foreach($catalogs as $catalog)
                    <a class="catalog_item-link" href="{{route('showcatalog', ['catalog' => $catalog])}}">
                        <div class="catalog_item">
                            <div class="catalog_text">
                                {{$catalog->name}}
                            </div>
                            <div class="catalog_img">
                                <img src="{{asset('assets/img/'.$catalog->image_1)}}" alt="">
                            </div>
                        </div>
                    </a>
                @endforeach
                    <a class="catalog_item-link" href="{{route('vahendid')}}">
                        <div class="catalog_item">
                            <div class="catalog_text">
                                Hooldusvahendid
                            </div>
                            <div class="catalog_img">
                                <img src="assets/img/K55.jpg" alt="">
                            </div>
                        </div>
                    </a>
            </div>
        </div>
    </section>


@endsection

