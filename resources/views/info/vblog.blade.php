@extends('layout.site')

@section('contant')

    <section class="videoblog">
        <div class="container">
            <div class="line"></div>
            <div class="videoblog_inner">
                @foreach($videos as $video)
                <div class="videoblog_item">
                    <div class="videoblog_name">
                        {{$video->name}}
                    </div>
                    <div class="videoblog_link">
                        {!! $video->link !!}
                    </div>
                </div>
                    @endforeach
            </div>
        </div>
    </section>

    @endsection
