@extends('layout.site')

@section('contant')

    <section class="juhendid">
        <div class="container">
            <div class="line"></div>
            <div class="juhendid_inner">
                @foreach($juhendid as $juhend)
                <div class="juhendid_item">
                <div class="juhendid_name">
                    {{$juhend->name}}
                </div>
                    <div class="juhendid_file">
                        <a href="{{asset('/assets/docs/'.$juhend->file)}}"><img src="{{asset('/assets/img/pdf.png')}}" alt=""></a>
                    </div>
                </div>
                    @endforeach
            </div>
        </div>
    </section>

    @endsection
