@extends('layout.site')

@section('contant')

<section class="rules">
    <div class="container">
        <div class="line"></div>
        <div class="rules_inner">
            <h3 id="rules_title">Esteetilisele proteesile hüvitise saamine Sotsiaalkindlustuseametist</h3>

            <p>Esteetilisele proteesile hüvituse saamine käib läbi <a href="https://www.sotsiaalkindlustusamet.ee/et/organisatsioon-kontaktid/blanketid#Abivahendite%20blanketid">erisuse taotlemise</a>, täites selleks erisuse taotlus. Taotlus erisuse saamiseks tuleb esitada sotsiaalkindlustusametile enne abivahendi soetamist.</p>

            <ul><h3>Erisuse taotlemiseks on vaja:</h3>
                <li>• Eriarsti tõendit kuhu on märgitud vajamineva abivahendi täpne nimetus ja ISO-kood või rehabilitatsiooniplaani, kus on kirjas vajaminev abivahend.</li>
                <li>• Isikliku abivahendi kaarti saad taotleda elektrooniliselt <a href="https://www.sotsiaalkindlustusamet.ee/et/abivahendi_kaardi_taotlus">siin</a> või kohapeal lähimas <a href="https://www.sotsiaalkindlustusamet.ee/et/organisatsioon-kontaktid/ska-klienditeenindused">Sotsiaalkindlustusameti esinduses</a>.</li>
                <li>• Lisada dokumentidele kaks võrdsetel alustel hinnapakkumist abivahendile erinevate ettevõtete poolt.</li>
            </ul>

            <p>Lisainfo ja abi soodustuse taotlemisel:</p>

            <p><a href="tel:+37255641270">55 641270</a> / <a href="mailto:info@rehabi.ee">info@rehabi.ee</a></p>

            <h3>Taotluse esitamine</h3>
            <p>Eriarsti tõendi , taotlusvormi ja hinnapakkumused saad edastada nii e-postiga aadressil <a href="mailto:info@sotsiaalkindlustusamet.ee">info@sotsiaalkindlustusamet.ee</a> kui posti teel aadressil Paldiski mnt 80, Tallinn, 15092 Tallinn.</p> 

            <h3>Kui kaua läheb aega otsuse tegemisele?</h3>
            <p>Sotsiaalkindlustusameti spetsialistid vaatavad sinu esitatud dokumendid üle ning vajadusel võtavad üksikasjade täpsustamiseks ka sinuga ühendust. Vastuse oma taotlusele saad 30 päeva jooksul.</p>

            <h3>Kuidas ma saan abivahendi kätte?</h3>
            <p>Positiivse otsuse korral võta abivahendi ettevõttesse kaasa lisaks otsusele isikliku abivahendi kaart ja isikut tõendav dokument.</p>
        </div>
    </div>
</section>

    @endsection
