<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, maximum-scale=1">
    <title>Rehabi</title>
    <link rel="icon" href="{{asset('assets/favicon.png')}}" type="image/png">
    <link rel="stylesheet" href="{{asset('assets/css/normalize.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/fonts.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/media.css')}}">


</head>
<body>
<section class="header">
    <div class="container">
        <div class="header_inner">
            <div class="header_logo">
                <a href="/"><img src="{{asset('assets/img/logo.png')}}" alt=""></a>
            </div>
            <nav class="menu">

                    <ion-icon name="menu-outline"class="header_menu_btn" id="header_menu_btn"></ion-icon>

                <ul class="header_menu" id="header_menu">
                    <li><a href="{{url('catalog')}}">Tootekataloog</a></li>
                    <li><a href="{{url('info')}}">Abiinfo</a></li>
                    <li><a href="{{url('contacts')}}">Kontakt/vastuvõtt</a></li>
                </ul>
                <div class="adaptive">
                <div class="lang_select">
                    <select>
                        <option value="" selected="selected">Est</option>
                        <option value="">Eng</option>
                        <option value="">Rus</option>
                    </select>
                </div>
                <div class="social-media">
                    <ul>
                        <li>
                            <a href="{{ url('http://facebook.com')}}"><img src="{{asset('assets/fonts/facebook-color.svg')}}" alt="">
                            </a>
                            <a href="{{ url('https://www.youtube.com/channel/UC8yfi1kM2FsVFWn3khdPbcw')}}"><img src="{{asset('assets/icons/youtube.svg')}}" alt="">
                            </a>
                            <a href="{{ url('https://www.instagram.com/rehabiaesthetics/')}}"><img src="{{asset('assets/fonts/insta-color.svg')}}" alt="">
                            </a>
                        </li>
                    </ul>
                </div>
                </div>
            </nav>
        </div>
    </div>
</section>

@yield('contant')

<section class="footer">
    <div class="container">
        <div class="footer_inner">
            <div class="footer_inner_contacts">
                <ul>
                    <li>Rehabi Aesthetics OÜ</li>
                    <li>
                        <ion-icon class="icons" name="location-outline"></ion-icon>
                        Tulika 19, Tallinn, 10613
                    </li>
                    <li>Avatud: E-R 10-17 L-P kokkuleppel</li>
                    <li>
                        <ion-icon class="icons" name="call-outline"></ion-icon>
                        <a href="tel:+37255641270">+372 55 64 12 70</a></li>
                    <li>
                        <ion-icon class="icons" name="mail-outline"></ion-icon>
                        <a href="mailto:info@rehabi.ee">info@rehabi.ee</a></li>
                    <li class="social-media_footer">
                        <a href="{{ url('http://facebook.com')}}"><img src="{{asset('assets/icons/facebook.svg')}}" alt=""> </a>
                        <a href="{{ url('https://www.youtube.com/channel/UC8yfi1kM2FsVFWn3khdPbcw')}}"><img src="{{asset('assets/icons/youtube.svg')}}" alt=""> </a>
                        <a href="{{ url('https://www.instagram.com/rehabiaesthetics/')}}"><img src="{{asset('assets/fonts/insta-color.svg')}}" alt=""> </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="{{asset('assets/js/ddslick.js')}}"></script>
<script src="{{asset('assets/js/script.js')}}"></script>

</body>
</html>
