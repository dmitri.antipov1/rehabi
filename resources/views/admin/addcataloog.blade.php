@php
    /**
     * @var \App\Catalog $catalogData
    */
@endphp


@extends('layout.admin_layout')

@section('contant')

<section class="data">
    <div class="data__inner">
        <div class="data__inner-title">
            Andmed
        </div>
        <table class="data__inner-table">
            <tr>
                <th>№</th>
                <th>Toode nimi</th>
                <th>Pilt</th>
                <th>Pea nimi</th>
                <th>Protsessid</th>
                <th>Pilt 2</th>
{{--                <th>Valmistamis etapid</th>--}}
                <th>Material</th>
                <th>Kinnitamisviisid</th>
                <th>Tühista</th>
            </tr>

            @foreach($catalogData as $k => $cData)
                <tr>
                    <td>{{$cData->id}}</td>
                    <td>{{$cData->name}}</td>
                    <td>{{$cData->image_1}}</td>
                    <td>{{$cData->title}}</td>
                    <td>{{$cData->protsessid}}</td>
                    <td>{{$cData->image_2}}</td>
{{--                    <td>{{$cData>val_etapid}}</td>--}}
                    <td>{{$cData->material}}</td>
                    <td>{{$cData->kinnitan}}</td>

                    <td>
                    {!! Form::open() !!}

                    {!! Form::hidden('action', 'delete') !!}
                    {!! Form::button('Tuhista', ['class'=>'btn-delete', 'type' => 'submit']) !!}

                    {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
</section>

    @endsection
