<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

class VahendidsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vahendids')->insert([
            [
                'name' => 'ALPS kaitsekreem',
                'img' => 'alps.png',
                'descr' => 'ALPS-i 100% silikoonist nahakreem aitab kuiva ja tundlikku nahka niisutada ja pehmendada. See on hüpoallergeenne, ei sisalda parfüüme ega värvaineid ega jäta pärast kasutamist õlist tunnet. ALPS  nahakreem kaitseb nahka liigse hõõrdumise eest ja aitab saavutada parema proteesi kinnitumise köndiga. Aitab silikoonist proteesi paigaldamisel (könt libiseb paremini proteesi sisse, hoiab ära nii naha kui proteesi vigastamise).',
                'hind' => '26 eur',
            ],
            [
                'name' => 'Proteesipuhastusvahend',
                'img' => 'Proteesipuhastusvahend.png',
                'descr' => 'ALPS-i proteesipuhastusvahend on leelise- ja seebivaba puhastusvahend, mis on loodud proteesilt higi eemaldamiseks. Patsiendid, kellel on tundlik nahk, võivad seda toodet kasutada lisaks veel kehapiirkondade pesemiseks, mis puutuvad kokku proteesiga. ',
                'hind' => '48 eur',
            ],
            [
                'name' => 'ALPS-i nahakreem',
                'img' => 'nahakreem.png',
                'descr' => 'ALPS-i A- ja D-vitamiini sisaldav nahakreem rahustab ja tugevdab nahka. Seda võib määrida peale ööseks või pärast proteesi eemaldamist. ',
                'hind' => ' eur',
            ],
            [
                'name' => 'Higistamisvastane sprei proteesikasutajatele',
                'img' => 'proteesikasutajatele.png',
                'descr' => 'ALPS-i antiperspirant on esimene omataoline, mis on mõeldud spetsiaalselt proteesikasutajatele.  See loodi spetsiaalselt selleks, et aidata proteesikasutajatele kõrvaldada ja kontrollida liigsest higistamisest tulenevaid probleeme. See sisaldab kaks korda suurema koguse alumiiniumklorohüdraati kui tavalised antiperspirandid, mis aitavad minimeerida naha pinnale tekkivat higi. ALPS-i ekstratugeva, ainulaadse  koostisega toode aitab kasutajatel kindlasti tunda ennast palju mugavalt ja kuivemalt.',
                'hind' => 'eur',
            ],
            [
                'name' => 'ALPS-i antioksüdantkreem',
                'img' => 'antioksudantkreem.png',
                'descr' => 'ALPS-i antioksüdantkreem kaitseb ärritunud nahka ning aitab vähendada hõõrdumist. Samuti omab nahka rahustavat toimet ning leevendab sügelust. Selle kasutamiseks kandke nahale õhuke kith, mis tagab kaitse kogu päevaks. ',
                'hind' => '28 eur',
            ],
            [
                'name' => 'ALPS libisemisvedelik ',
                'img' => 'libisemisvedelik.png',
                'descr' => 'ALPS libisemisvedelik on kiiresti aurustuv vedelik, mis teeb proteesi paigaldamise lihtsamaks. Soovitame kasutada iga kord kui proteesi paigaldate. Proteesi libisemisvedeliku eesmärk on kaitsta nahka hõõrdumise eest ning pikendada proteesi eluiga. Kui proteesi paigaldada ilma seda kasutamata siis suureneb oluliselt oht, et proteesi äär läheb katki. ',
                'hind' => '21 eur',
            ],
            [
                'name' => 'Pros-Aide proteesiliim',
                'img' => 'proteesiliim.png',
                'descr' => 'Nahaga kasutamiseks mõeldud ja vee peal põhinev liim, mis liimib pikemaks perioodiks ilma ärrituseta. Pros-Aide on esteetilise proteeside puhul enimkasutatud liim. Seda on ohutu kasutada ka isegi tundlikes piirkondades. See tagab tugeva kinnitumise ja on kõrge veekindlusega, mittetoksiline ja täiesti ohutu. ',
                'hind' => '36 eur',
            ],
            [
                'name' => 'PROS-AIDE liimi eemaldaja',
                'img' => 'PROS-AIDE.png',
                'descr' => 'Spetsiaalne liimi eemalduse/puhastuse vahend, mis tagab ohutu Pros-Aide liimi eemaldamise nahalt. ',
                'hind' => '58 eur',
            ],

        ]);
    }
}
