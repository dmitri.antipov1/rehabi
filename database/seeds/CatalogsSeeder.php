<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatalogsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('catalogs')->insert([
            [
            'name' => 'Sõrmeproteesid',
            'image_1' => 'K11.jpg',
            'protsessid' => '<p>Sõrme või sõrmeosade kaotus on kõige levinum ülajäsemete kaotuse vorm. Sõrme kaotus on tavaliselt tingitud traumaatilisest vigastusest, kuid võib olla tingitud ka kaasasündinud väärarengust või haigusest. Sõltumata sõrme kaotuse põhjusest, on sellel negatiivne mõju inimese funktsionaalsusele, igapäevaelu tegevustele ja psühhosotsiaalsele heaolule. Iga sõrmeprotees on individuaalselt valmistatud tellimusmeditsiiniseade. Selleks et sõrmeprotees oleks funktsionaalne, esteetiline ja sarnane teiste sõrmega, osalevad  valmistamise protsessis proteesimeister, skulptor ja kunstnik. </p>',
            'image_2' => 'PALETZ.jpg',
            'val_etapid' => '<ul><h3>Sõrmeproteesi valmistamise etapid</h3>
	                        <li>Jäljendi võtmine</li>
	                        <li>Sõrmeproteesi valmistamine</li>
	                        <li>Proovimine</li>
	                        <li>Proteesi individuaalne värvimine</li>
                            </ul>',
            'material' => '<ul><h3>Materjal</h3>
                            <li>Meditsiiniline RTV silikoon. Valminud sõrmeprotees on hüpoallergeeniline ja nahaga kokkupuutel ohutu.</li></ul>',
                'kinnitan' => '<ul><h3>Kinnitamisviisid<h3>
	                        <li>Vaakumiga</li>
	                        <li>Meditsiinilise liimiga <i>(Pros-Aide Adhesive)</i></li>
                            </ul>

                            <p>Sobiva kinnitamisviisi aitab valida meie proteesimeister, arvestades kliendi soovidega, sõrme kaotuse taseme ja köndi kujuga. </p>',
                'title' => '<h3>Sõrmeproteesid</h3>'
            ],

            [
                'name' => 'Käeosade proteesid',
                'image_1' => 'K22.jpg',
                'protsessid' => '<p>Kõik meie proteesid on individuaalselt valmistatud ja Terviseametis resgistreeritud tellimusmeditsiiniseadmed. Tänapäevane tehnoloogia võimaldab valmistada käeosade proteese , mis immiteerivad identselt  teie loomuliku nahatooni,  jäsemete kuju, küüni, kortsusidd, karvu ja soovi korral isegi ka tätoveeringuid. Esteetilised käeosade proteesid valmistatakse meditsiinilisest silikoonist ja liigitatakse passiivse funktsiooniga proteeside hulka. Esteetilise käeosa proteesi primaarne ülesanne on parandada  inimeste psühhosotsiaalse heaolu, asendades puuduvat käeosa. Teiseks ülesandeks on käe osaline funktsionaalsuse taastamine. Esteetiline protees aitab haarata, tõsta, hoida  ja nihutada esemeid. </p>',
                'image_2' => 'RUKA.jpg',
                'val_etapid' => '<ul><h3>Käeosade proteesi valmistamise etapid</h3>
	<li>Jäljendi võtmine</li>
	<li>Proteesi valmistamine</li>
	<li>Proovimine</li>
	<li>Proteesi värvimine</li>
</ul>',
                'material' => '<ul><h3>Materjal</h3>
<li>Meditsiiniline RTV silikoon. Valminud käeosade protees on hüpoallergeeniline ja nahaga kokkupuutel ohutu.</li></ul>',
                'kinnitan' => '<ul><h3>Kinnitamisviisid</h3>
	<li>Vaakumiga</li>
	<li>Meditsiinilise liimiga (Pros-Aide Adhesive)</li>
</ul>',
                'title' => '<h3>Käeosade proteesid</h3>'
            ],
            [
                'name' => 'Jalaosade proteesid',
                'image_1' => 'K33.jpg',
                'protsessid' => '<p>Jalaosade prtoteeside huulka kuluvad kõik proteesid, mis asendavad kaotatud labajala osa varvastest hüpeliigesteni. Sõltumata labajala kaotuse tasemest, on sellel negatiivne mõju inimese funktsionaalsusele, igapäevaelu tegevustele ja psühhosotsiaalsele heaolule. Jalaosa puudumine mõjutab inimsesel kõnnimustrit , hakkab lonkama, tasakaal muutub ebakindlaks ja tekivad valud nii jalas kui seljas. Tänapäeval on olemas meditsiinilise klassi materjalid, silikoon või polüuretaan, millest valmistatud proteesid kompenseerivad kaotatud jalaosade puudumise ning taastavad olulisel määral jala funktsiooni. </p>',
                'image_2' => 'NOGA.jpg',
                'val_etapid' => '<ul><h3>Jalaosade proteesi valmistamis etapid</h3>
	<li>Jäljendi võtmine</li>
	<li>Proteesi  valmistamine</li>
	<li>Proovimine</li>
	<li>Proteesi värvimine (vajadusel)</li>
</ul>',
                'material' => '<ul><h3>Materjal</h3>
	<li>Meditsiiniline RTV silikoon</li>
	<li>Polüuretaan kumm</li>
</ul>',
                'kinnitan' => '<p>Valmis protees on hüpoallergeeniline ja nahaga kokkupuutel ohutu. Sobiva materjali aitab valida meie proteesimeister, arvestades sealjuures kliendi soovidega, labajala kaotuse taseeme ja köndi kujuga. </p>

<ul>Kinnitamisviis
	<li>Vaakumiga</li>
</ul>',
                'title' => '<h3>Jalaosade proteesid</h3>'
            ],
            [
                'name' => '3D lahendused',
                'image_1' => 'K44.jpg',
                'protsessid' => '<p>Tänu olulistele arengutele 3D-printimise tehnoloogias on tänapäeval võimalik modelleerida ja valmistada proteesimises ja abivahendite tootmises selliseid 3D lahendusi, mida konservatiivne tehnoloogia ei võimalda. </p>',
                'image_2' => '3D.jpg',
                'val_etapid' => '<ul><h3>Valmistamis etapid</h3>
	<li>3D projekteerimine</li>
	<li>3D Printimine</li>
	<li>Proovimine</li>
	<li>Individuaalne</li>
</ul>',
                'material' => '<ul><h3>Materjalid</h3>
<li>Materjalid, mida me meditsiiniseadmete valmistamisel kasutame on põhjalikult uuritud ja neid kasutatakse laialdaselt laste mänguasjade, söömis-, joomis- ja hügieenitarvikute tootmises. </li>
<li>ABS ehk akrüülonitriilbutadieenstüreenplast - akrüülnitriidist, butadieenist ja stüreenist koosnev kopolümeer. ABS-ist on valmistatud Legode klotsid või ka näiteks tavaline arvuti hiir.</li>
<li>PLA ehk polüpiimhappe termoplast on toodetud taimsetest komponentidest. PLA on  pärit bioloogilistest ressurssidest ja on seega biolagunev materjal.</li>
<li>HIPS ehk polüstüreen on termoplast, mis on keemiliselt inertne (ei reageeri hapete ja alustega). Tänu oma vastupidavusele ja inertsusele on see enim kasutatud termoplast polümeer maailmas. Seda kasutatakse laialdaselt toiduainetööstuses, aga  ka samuti söögiriistade, kausside jne tootmisel.</li></ul>',
                'kinnitan' => 'null',
                'title' => '<h3>3D-printimise tehnoloogia</h3>'
            ]

            ]);
    }
}
