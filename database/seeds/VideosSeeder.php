<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

class VideosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('videos')->insert([
            [
                'name' => 'Esteetilise käeproteesi värvimine',
                'link' => 'https://www.youtube.com/watch?v=glrJl40WkSo'
            ],
            [
                'name' => 'Esteetiline käeprotees liigutatavate sõrmedega',
                'link' => 'https://www.youtube.com/watch?v=XERmN7QXjuI'
            ],
            [
                'name' => '3D käeprotees lapsele',
                'link' => 'https://www.youtube.com/watch?v=AfXKYnytLbM'
            ]

        ]);
    }
}
