<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JuhendidsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('juhendids')->insert([

            [
                'name' => 'Sõrm hooldus',
                'file' => 'HOOLDUS_SORM.pdf'
            ],
            [
                'name' => 'Käeosa hooldus',
                'file' => 'HOOLDUS_KASI.pdf'
            ],
            [
                'name' => 'Jala hooldus',
                'file' => 'HOOLDUS_JALG.pdf'
            ]

        ]);
    }
}
