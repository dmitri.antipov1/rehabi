<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CatalogsSeeder::class);
        $this->call(JuhendidsSeeder::class);
        $this->call(VahendidsSeeder::class);
        $this->call(VideosSeeder::class);
    }
}
